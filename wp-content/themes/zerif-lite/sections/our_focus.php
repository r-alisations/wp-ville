<?php
/**
 * Our Focus section
 *
 * @package zerif-lite
 */

zerif_before_our_focus_trigger();

$zerif_ourfocus_show = get_theme_mod( 'zerif_ourfocus_show' );

echo '<section class="focus ' . ( ( is_customize_preview() && ( ! isset( $zerif_ourfocus_show ) || $zerif_ourfocus_show == 1 ) ) ? ' zerif_hidden_if_not_customizer ' : '' ) . '" id="focus">';

?>

	<?php zerif_top_our_focus_trigger(); ?>

	<div class="container">

		<!-- SECTION HEADER -->

		<div class="section-header">

			<!-- SECTION TITLE  -->

			<h2 class="dark-text">
				La commune
			</h2>

		</div>

		<p class="focus-block-text">
			Nullam mauris nibh, hendrerit a lacus non, vestibulum suscipit magna. Vivamus euismod urna ut massa sodales dapibus. Cras accumsan lectus a pharetra dictum. Sed rutrum, mi nec finibus pretium, turpis elit euismod libero, iaculis congue augue dolor eget eros. Mauris aliquet augue quis mauris accumsan, id lacinia nisi condimentum. Maecenas pellentesque, dolor a congue commodo, metus magna egestas odio, vel placerat neque orci et magna. Morbi fermentum laoreet dui. Ut sagittis dictum aliquam. Sed pellentesque scelerisque est, a aliquam felis vestibulum in.
		</p>

		<p class="focus-block-text">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet nibh blandit, porttitor nunc et, interdum nisi. Aliquam ligula mi, vestibulum at sem id, mattis sodales eros. Maecenas vestibulum ut nulla nec convallis. Fusce ut quam ac quam egestas vestibulum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc in consectetur leo. Curabitur vitae felis sed lacus scelerisque vulputate. Vestibulum fermentum vel turpis ac rhoncus. Sed dignissim varius magna sit amet gravida. Duis vestibulum mauris rutrum, vulputate magna quis, aliquet est. In non magna nec massa sagittis venenatis. Ut ultricies purus eros, nec sagittis libero egestas a. In velit eros, tempor ac purus sed, facilisis imperdiet erat. Suspendisse maximus cursus nulla vitae iaculis.

		</p>

		<div class="focus-block-text-img right first">
			<img src="<?php echo get_template_directory_uri(); ?>/images/focus1.jpg" alt="" class="focus-block-img">
			<p class="focus-block-text">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet nibh blandit, porttitor nunc et, interdum nisi. Aliquam ligula mi, vestibulum at sem id, mattis sodales eros. Maecenas vestibulum ut nulla nec convallis. Fusce ut quam ac quam egestas vestibulum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc in consectetur leo. Curabitur vitae felis sed lacus scelerisque vulputate. Vestibulum fermentum vel turpis ac rhoncus. Sed dignissim varius magna sit amet gravida. Duis vestibulum mauris rutrum, vulputate magna quis, aliquet est. In non magna nec massa sagittis venenatis. Ut ultricies purus eros, nec sagittis libero egestas a. In velit eros, tempor ac purus sed, facilisis imperdiet erat. Suspendisse maximus cursus nulla vitae iaculis.
			</p>
		</div>

		<div class="focus-block-text-img left">
			<img src="<?php echo get_template_directory_uri(); ?>/images/focus2.jpg" alt="" class="focus-block-img">
			<p class="focus-block-text">
				Nullam mauris nibh, hendrerit a lacus non, vestibulum suscipit magna. Vivamus euismod urna ut massa sodales dapibus. Cras accumsan lectus a pharetra dictum. Sed rutrum, mi nec finibus pretium, turpis elit euismod libero, iaculis congue augue dolor eget eros. Mauris aliquet augue quis mauris accumsan, id lacinia nisi condimentum. Maecenas pellentesque, dolor a congue commodo, metus magna egestas odio, vel placerat neque orci et magna. Morbi fermentum laoreet dui. Ut sagittis dictum aliquam. Sed pellentesque scelerisque est, a aliquam felis vestibulum in.
			</p>
		</div>

	</div> <!-- / END CONTAINER -->

	<?php zerif_bottom_our_focus_trigger(); ?>

</section>  <!-- / END FOCUS SECTION -->

<?php zerif_after_our_focus_trigger(); ?>
